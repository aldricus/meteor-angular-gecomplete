/**
 * Simple angularJs directive for Jquery Geocomplete plugin (https://github.com/ubilabs/geocomplete)
 *
 * @author Aldric T <aldric.dev@gmail.com>, 2015
 * @author Aldricus, 2015
 * @license MIT License <http://www.opensource.org/licenses/mit-license.php>
 *
The MIT License
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

(function(window, angular, undefined) {
	'use strict';
	(function() {
		angular.module('placesGeocomplete', [
			'placesGeocompleteConfig',
			'uiGmapgoogle-maps',
			'ui.router'
		]).directive('uiGeocomplete', [
			'uiGmapGoogleMapApi',
			'placesGeocompleteOpt',
			'$timeout',
			function(GmapGoogleMapApi, PlacesGeoOpt, $timeout) {
				return {
					restrict: 'A,C',
					replace: true,
					transclude: false,
					compile: function(element, attrs) {
						return function(scope, element, attrs) {
							PlacesGeoOpt.eventHanlders = {
								'geocode:result': function(ev, result) {
									scope.geocompleteResult = result;
								}
							};
							// uiGmapGoogleMapApi is a promise so the 'then' callback
							// function provides the google.maps object ready
							angular.extend(PlacesGeoOpt.eventHanlders, PlacesGeoOpt.eventHanlders);
							GmapGoogleMapApi.then(function(maps) {
								element.geocomplete(PlacesGeoOpt.options)
									.bind(PlacesGeoOpt.eventHanlders);
							});
						}
					}
				};
			}
		]);
	}).call(this);
	(function() {
		angular.module('placesGeocompleteConfig', []).provider('placesGeocompleteOpt',
			function() {
				this.options = {
					/*key: 'your api key',
					v: '3.20',
					libraries: 'places'*/
				};
				this.configure = function(options, eventHanlders) {
					this.eventHanlders = (eventHanlders !== undefined) ? eventHanlders : {};
					angular.extend(this.options, options);
				};
				this.$get = function() {
					return {
						options: this.options,
						eventHanlders: this.eventHanlders
					};
				}
			}
		);
	}).call(this);
}(window, angular));