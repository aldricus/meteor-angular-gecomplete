Package.describe({
  name: 'aldricus:geocomplete',
  version: '0.0.1',
  summary: 'AngularJs directive for JQUERY Geocomplete plugin',
  git: '',
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.0.2');
  api.use('ecmascript');
  api.use('angular:angular@1.2.0');
  api.use('angularui:angular-ui-router');
  api.use('angularui:angular-google-maps');
  api.addFiles(['geocomplete.css', 'vendor/jquery.geocomplete.js', 'geocomplete.js'], 'client');
});

Package.onTest(function(api) {
  /*api.use('ecmascript');
  api.use('tinytest');
  api.use('aldricus:geocomplete');
  api.addFiles('geocomplete-tests.js');*/
});
